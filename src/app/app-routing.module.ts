import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './views/home/home.component';
import { PromocoesComponent } from './views/promotions/promocoes.component';
import { TermosComponent } from './views/termos/termos.component';
import { StoreComponent } from './views/store/store.component';
import { DealerComponent } from './views/dealer/dealer.component';
import { CompanyComponent } from './views/company/company.component';
import { SearchComponent } from './views/search/search.component';
import { SignupComponent } from './views/signup/signup.component';

import { StoreHomeComponent } from './views/store-home/store-home.component';
import { StoreProductComponent } from './views/store-product/store-product.component';
import { StorePromotionsComponent } from './views/store-promotions/store-promotions.component';
import { StoreCommentsComponent } from './views/store-comments/store-comments.component';

import { UserComponent } from './views/user/user.component';
import { UserMydelysComponent } from './views/user-mydelys/user-mydelys.component';
import { UserProfileComponent } from './views/user-profile/user-profile.component';
import { UserCouponsComponent } from './views/user-coupons/user-coupons.component';
import { UserRescuesComponent } from './views/user-rescues/user-rescues.component';
import { UserCommentsComponent } from './views/user-comments/user-comments.component';



const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'promocoes', component: PromocoesComponent },
  { path: 'termos', component: TermosComponent },
  { path: 'representante', component: DealerComponent },
  { path: 'empresa', component: CompanyComponent },
  { path: 'cadastro', component: SignupComponent },
  { path: 'loja/:storeId', component: StoreComponent },
  { path: 'busca/:search', component: SearchComponent },
  { path: 'busca', component: SearchComponent },
  { path: 'usuario', component: UserComponent,
    children: [
      { path: '', component: UserMydelysComponent },
      { path: 'perfil', component: UserProfileComponent },
      { path: 'cupons', component: UserCouponsComponent },
      { path: 'resgate', component: UserRescuesComponent },
      { path: 'comentarios', component: UserCommentsComponent },
    ],
  },

  { path: ':storeId', component: StoreComponent,
    children: [
      { path: '', component: StoreHomeComponent },
      { path: 'produtos', component: StoreProductComponent },
      { path: 'promocoes', component: StorePromotionsComponent },
      { path: 'comentarios', component: StoreCommentsComponent },
    ],
  },
  
  { path: ':storeId', component: StoreComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
