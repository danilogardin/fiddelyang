import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './views/home/home.component';
import { PromocoesComponent } from './views/promotions/promocoes.component';
import { TermosComponent } from './views/termos/termos.component';
import { HeaderComponent } from './views/header/header.component';
import { FooterComponent } from './views/footer/footer.component';
import { HomeSliderPrimaryComponent } from './views/home-slider-primary/home-slider-primary.component';
import { HomeSliderSecondaryComponent } from './views/home-slider-secondary/home-slider-secondary.component';
import { HomeSliderTertiaryComponent } from './views/home-slider-tertiary/home-slider-tertiary.component';
import { HomeSliderQuaternaryComponent } from './views/home-slider-quaternary/home-slider-quaternary.component';
import { StoreComponent } from './views/store/store.component';
import { CompanyComponent } from './views/company/company.component';
import { DealerComponent } from './views/dealer/dealer.component';
import { SearchComponent } from './views/search/search.component';
import { HeaderNavComponent } from './views/header-nav/header-nav.component';
import { HeaderRegionComponent } from './views/header-region/header-region.component';
import { HeaderRegionModalComponent } from './views/header-region-modal/header-region-modal.component';
import { HeaderCategoriesComponent } from './views/header-categories/header-categories.component';
import { CityService } from './services/city.service';
import { LoginService } from './services/login.service';
import { SignupService } from './services/signup.service';
import { ModalComponent } from './views/modal/modal.component';
import { SignupComponent } from './views/signup/signup.component';
import { LoginComponent } from './views/login/login.component';
import { commonUser } from './base/common';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatInputModule, MatAutocompleteModule } from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { StoreHomeComponent } from './views/store-home/store-home.component';
import { StoreProductComponent } from './views/store-product/store-product.component';
import { StorePromotionsComponent } from './views/store-promotions/store-promotions.component';
import { StoreCommentsComponent } from './views/store-comments/store-comments.component';
import { UserComponent } from './views/user/user.component';
import { UserMydelysComponent } from './views/user-mydelys/user-mydelys.component';
import { UserProfileComponent } from './views/user-profile/user-profile.component';
import { UserCouponsComponent } from './views/user-coupons/user-coupons.component';
import { UserRescuesComponent } from './views/user-rescues/user-rescues.component';
import { UserCommentsComponent } from './views/user-comments/user-comments.component';
import { UserMydelysListComponent } from './views/user-mydelys-list/user-mydelys-list.component';
import { UserMydelysDiscountComponent } from './views/user-mydelys-discount/user-mydelys-discount.component';
import { FooterCategoryComponent } from './views/footer-category/footer-category.component';
import { FooterCityComponent } from './views/footer-city/footer-city.component';
import { FooterNavigationComponent } from './views/footer-navigation/footer-navigation.component';
import { PromotionsFilterComponent } from './views/promotions-filter/promotions-filter.component';
import { PromotionsDetachComponent } from './views/promotions-detach/promotions-detach.component';
import { PromotionsListComponent } from './views/promotions-list/promotions-list.component';
import { CompanyFormComponent } from './views/company-form/company-form.component';
import { DealerFormComponent } from './views/dealer-form/dealer-form.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PromocoesComponent,
    TermosComponent,
    HeaderComponent,
    FooterComponent,
    HomeSliderPrimaryComponent,
    HomeSliderSecondaryComponent,
    HomeSliderTertiaryComponent,
    HomeSliderQuaternaryComponent,
    StoreComponent,
    CompanyComponent,
    DealerComponent,
    SearchComponent,
    HeaderNavComponent,
    HeaderRegionComponent,
    HeaderCategoriesComponent,
    ModalComponent,
    SignupComponent,
    LoginComponent,
    HeaderRegionModalComponent,
    StoreHomeComponent,
    StoreProductComponent,
    StorePromotionsComponent,
    StoreCommentsComponent,
    UserComponent,
    UserMydelysComponent,
    UserProfileComponent,
    UserCouponsComponent,
    UserRescuesComponent,
    UserCommentsComponent,
    UserMydelysListComponent,
    UserMydelysDiscountComponent,
    FooterCategoryComponent,
    FooterCityComponent,
    FooterNavigationComponent,
    PromotionsFilterComponent,
    PromotionsDetachComponent,
    PromotionsListComponent,
    CompanyFormComponent,
    DealerFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [CityService, LoginService, commonUser, SignupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
