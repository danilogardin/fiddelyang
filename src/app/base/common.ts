import { Injectable } from '@angular/core';
import { City } from '../models/city.model';

@Injectable()
export class commonUser {
    
	public usrName: String = '';
    public userClientStatus: boolean = false;
    public usrCity: String = '';

    constructor() {	
		this.userClientStatus = this.getUserLoginState() ? true : false;
		this.usrName = this.getUserName();	
    }

	lstCity = new City();

    // USER
    getUserToken() {
        return this.getUserLoginState() ? JSON.parse(localStorage.getItem('userClient')).token as String : null;
    }
    getUserName() {
        return this.getUserLoginState() ? JSON.parse(localStorage.getItem('userClient')).name as String : null;
    }
    
    // REGION
    regionInit() {
        if (this.checkUserCity()) {
            this.setUserCity(this.getUserCity().city1);
		} else {
            this.setUserCity('');
		}
    }
    checkUserCity() {
		return JSON.parse(localStorage.getItem('userCity')) != null as boolean;
    }
    getUserCity() {
		return JSON.parse(localStorage.getItem('userCity')) as any;
    }
    setUserCity(c: string) {
        if (this.checkUserCity()) {
            let oUser = this.getUserCity();
                oUser.city1 = c;
            this.setUserCityStorage(oUser);
            console.log('city update');
        } else {
            console.log('city new');
            this.createUserCity('');
        }
        this.usrCity = this.getUserCity().city1;
    }
    createUserCity(c: string){
		this.lstCity.cityId = 0;
		this.lstCity.city1 = c == '' ? 'São Paulo' : c;
		this.lstCity.stateId = 'SP';
		this.lstCity.state = 'SP';
		this.lstCity.banner = '';
		this.lstCity.companyStore = '';
		this.setUserCityStorage(this.lstCity);
	}
    setUserCityStorage(o : any){
        localStorage.setItem('userCity', JSON.stringify( o ));
    }


    // LOGIN
    getUserLoginState() {
        return JSON.parse(localStorage.getItem('userClient')) != null;
    }
	onUserLogout() {
		localStorage.removeItem('userClient');
        this.userClientStatus = false;
    }
	setUserLogged() {
		this.userClientStatus = this.getUserLoginState() ? true : false;
		this.usrName = this.getUserName();
	}

    // LOCALSTORAGE
    setUserClientStorage(o : any){
        localStorage.setItem('userClient', JSON.stringify( o ));
    }
}

