export class Signup {

    constructor(cpf: string, facebookId: string, password: string, hash: string, firstName: string, lastName: string, sex: string, birthday: string, telephone: string, email: string, cityId: number, token: string) {
        this.cpf = cpf;
        this.facebookId = facebookId;
        this.password = password;
        this.hash = hash;
        this.firstName = firstName;
        this.lastName = lastName;
        this.sex = sex;
        this.birthday = birthday;
        this.telephone = telephone;
        this.email = email;
        this.cityId = cityId;
        this.token = token;
    }
  
    cpf: string;
    facebookId: string;
    password: string;
    hash: string;
    firstName: string;
    lastName: string;
    sex: string;
    birthday: string;
    telephone: string;
    email: string;
    cityId: number;
    token: string;
  }