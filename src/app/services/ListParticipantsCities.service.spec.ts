import { TestBed } from '@angular/core/testing';

import { ParticipantsService } from './ListParticipantsCities.service';

describe('ParticipantsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParticipantsService = TestBed.get(ParticipantsService);
    expect(service).toBeTruthy();
  });
});
