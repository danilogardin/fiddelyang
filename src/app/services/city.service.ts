import { Injectable } from '@angular/core';
import { ServiceBase } from './service-base';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';
//import { map } from 'rxjs/operators';


@Injectable({
	providedIn: 'root'
})
export class CityService extends ServiceBase { 

	constructor( public http: HttpClient) { 
		super(http); 
	}
	
	list(pKey: string) : Observable<any> {
		var paramKey = new HttpParams().set('keyword', pKey);
		const url = `${this.BASE_URL}/general/listcity/`;
		//console.log( `Token: ${this.getToken()} `);

		return this.http.get(url, {params: paramKey});
	}
	
}
