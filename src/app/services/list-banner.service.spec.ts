import { TestBed } from '@angular/core/testing';

import { ListBannerService } from './list-banner.service';

describe('ListBannerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListBannerService = TestBed.get(ListBannerService);
    expect(service).toBeTruthy();
  });
});
