import { Injectable } from '@angular/core';

import { ServiceBase } from './service-base';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListBannerService extends ServiceBase {

	constructor( public http: HttpClient) { 
		super(http); 
		
	}


	listBanner() : Observable<any> {
    
		var paramKey = new HttpParams();
		const url = `${this.BASE_URL}/home/`;
		//console.log( `Token: ${this.getToken()} `);
		
		return this.http.get(url, {params: paramKey});
	}
	
}
