import { Injectable } from '@angular/core';
import { ServiceBase } from './service-base';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Login } from '../models/login.model';

@Injectable({
	providedIn: 'root'
})

export class LoginService extends ServiceBase { 

	constructor( public http: HttpClient) { 
		super(http); 
	}
	
	onLogin(objLogin: any) : Observable<any> {
		var paramKey = new HttpParams().set('cpf', objLogin.pCPF).set('facebookId', objLogin.pFacebook).set('password', objLogin.pPassword).set('email', objLogin.pEmail);
		//var paramKey = { cpf: '121.121.121-55', 'facebookId':'', 'password':'', 'email':'' };
		/*const url = `${this.BASE_URL}/user/login/`;
		console.log( `${paramKey} `);

		return this.http.post(url, {params: paramKey});*/

		return this.http.post<Login>(`${this.BASE_URL}/user/login/`, paramKey);
	}
		
}
