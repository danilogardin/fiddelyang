
import { Settings } from '../settings';
import { commonUser } from '../base/common';
import { Observable } from 'rxjs';
import { HttpClient , HttpEvent, HttpHandler, HttpHeaders, HttpRequest} from '@angular/common/http';

export class ServiceBase extends commonUser {

 	public BASE_URL = Settings.BASE_URL;
 	public EMAIL_URL = Settings.EMAIL_URL;
 	public ANALYTICS_URL = Settings.ANALYTICS_URL;
 	public DEBUG = '?XDEBUG_SESSION_START=PHPSTORM';

  	constructor(public  http: HttpClient) {
		  super();
  	}

 	public header() {

		const _headers: HttpHeaders = new HttpHeaders();
		_headers.append('Content-Type', 'application/json');

		const token = this.getUserToken();

		if (token != null) {
			_headers.append('Authorization', 'Bearer ' + token);
		}

		return {headers: _headers};

  	}

	public onError(error: Response | any) {
		return Observable.throw(error.message || error);
  	}

  	public extractData(res: any) {
		// console.log(res.status)
		const body = res.json();
		return body || {};
  	}

}
