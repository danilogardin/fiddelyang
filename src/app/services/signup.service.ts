import { Injectable } from '@angular/core';
import { ServiceBase } from './service-base';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Signup } from '../models/signup.model';

@Injectable({
  providedIn: 'root'
})
export class SignupService extends ServiceBase {

  constructor( public http: HttpClient) { 
		super(http); 
	}
  setSignUp(obj: any) : Observable<any> {
    return this.http.post<Signup>(`${this.BASE_URL}/user/add/`, JSON.stringify(obj) );
  }
}
