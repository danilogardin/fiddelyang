// noinspection TsLint

export class Settings {

        //public static BASE_URL = ' http://fiddely.produtoraduka.com.br';
        //public static BASE_URL = ' http://localhost:4200';  
        public static BASE_URL = 'http://fiddely-api.produtoraduka.com.br/api/v1';
    
        //public static EMAIL_URL = ' http://mail.produtoraduka.com.br';
        public static EMAIL_URL = ' http://localhost:4200';  
      
        public static ANALYTICS_URL = 'http://hmanalytics.produtoraduka.com.br/api/Analytics';
    
        constructor() {
        }

    }
    