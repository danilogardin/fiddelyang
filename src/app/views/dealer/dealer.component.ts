import { Component, OnInit } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-dealer',
  templateUrl: './dealer.component.html',
  styleUrls: ['./dealer.component.css']
})
export class DealerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.accordeon();
  }

  accordeon() {
		var element = $(".js-accordeon");
		if (element[0]) {
			element.find(".accordeon-question").on("click", "a", function(e) {
				e.preventDefault();
				e.stopImmediatePropagation();
				//  console.log('clicado accordeon');
				$(this)
					.parent()
					.toggleClass("opened");
				$(this)
					.parent().parent()
					.toggleClass("opened");
				$(this)
					.closest(".accordeon-item")
					.find(".accordeon-anwser")
					.stop()
					.slideToggle();
				$(this)
					.closest(".accordeon-item")
					.find(".accordeon-question")
					.find(".ico")
					.toggleClass("clicked");
				$(this)
					.closest(".accordeon-item").siblings().removeClass('opened').find('.accordeon-question').removeClass('opened').find('.ico').removeClass('clicked');
				$(this)
					.closest(".accordeon-item").siblings().find('.accordeon-anwser').stop().slideUp();
				return false;
			});

		}
	}

}
