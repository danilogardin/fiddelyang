import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterCityComponent } from './footer-city.component';

describe('FooterCityComponent', () => {
  let component: FooterCityComponent;
  let fixture: ComponentFixture<FooterCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
