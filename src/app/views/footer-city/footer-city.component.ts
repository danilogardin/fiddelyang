import { Component, OnInit } from '@angular/core';

import { ParticipantsService } from '../../services/ListParticipantsCities.service';
import { commonUser } from '../../base/common';

@Component({
  selector: 'app-footer-city',
  templateUrl: './footer-city.component.html',
  styleUrls: ['./footer-city.component.css']
})
export class FooterCityComponent implements OnInit {
	

  	constructor( private _ParticipantsService : ParticipantsService, public _commonUser : commonUser) {
		//console.log('header region');
	}

	cityOptions: any;
  
	ngOnInit() {

		this.getServiceCity();
		
	}


	getServiceCity(){

		this._ParticipantsService.list().subscribe(
			data => {
				this.cityOptions = data;
				
			}
		);
	}

}
