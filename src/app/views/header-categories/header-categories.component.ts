import { Component, OnInit } from '@angular/core';

import { MainCategoryService } from '../../services/main-category.service';
import { commonUser } from '../../base/common';

@Component({
  selector: 'app-header-categories',
  templateUrl: './header-categories.component.html',
  styleUrls: ['./header-categories.component.css']
})
export class HeaderCategoriesComponent implements OnInit {

  constructor( private _MainCategoryService : MainCategoryService, public _commonUser : commonUser) {
		//console.log('header region');
	}

	categoryOptions: any;
  
	ngOnInit() {

		this.getServiceCategory();
		
	}


    getServiceCategory(){

        this._MainCategoryService.list().subscribe(
            data => {
              this.categoryOptions = data.category;
              
            }
        );
    }

}
