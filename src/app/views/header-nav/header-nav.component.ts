import { Component, OnInit } from '@angular/core';
import { commonUser } from '../../base/common';
//import { Router } from '@angular/router';

declare var abreModal : any;

@Component({
	selector: 'app-header-nav',
	templateUrl: './header-nav.component.html',
	styleUrls: ['./header-nav.component.css']
})
export class HeaderNavComponent {

	constructor(public _commonUser : commonUser) { }

	openModalLogin(){
		abreModal('#modalLogin');
	}

	onLogout() {
		this._commonUser.onUserLogout();
	}

}
