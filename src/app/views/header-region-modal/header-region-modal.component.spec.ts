import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderRegionModalComponent } from './header-region-modal.component';

describe('HeaderRegionModalComponent', () => {
  let component: HeaderRegionModalComponent;
  let fixture: ComponentFixture<HeaderRegionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderRegionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderRegionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
