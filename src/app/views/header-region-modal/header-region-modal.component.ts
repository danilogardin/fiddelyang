import { Component, OnInit } from '@angular/core';
import { FormControl } from "@angular/forms";
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

export interface User {
	name: string;
}

@Component({
	selector: 'app-header-region-modal',
	templateUrl: './header-region-modal.component.html',
	styleUrls: ['./header-region-modal.component.css']
})

export class HeaderRegionModalComponent implements OnInit {

	myControl = new FormControl();
	options: string[] = ['One', 'Two', 'Three'];
	filteredOptions: Observable<string[]>;
  
	ngOnInit() {
	  this.filteredOptions = this.myControl.valueChanges
		.pipe(
		  startWith(''),
		  map(value => this._filter(value))
		);
	}
  
	private _filter(value: string): string[] {
	  const filterValue = value.toLowerCase();
  
	  return this.options.filter(option => option.toLowerCase().includes(filterValue));
	}
	

	cityTerms : any;


	searchOnKeyUp(event: KeyboardEvent) {
		//console.log(event);
		this.cityTerms = (<HTMLInputElement>event.target).value;
		console.log('execute ' + this.cityTerms);
	}

	searchExecute(word: string) {
		this.cityTerms = word;
		console.log('execute ' + this.cityTerms);
	}

}
