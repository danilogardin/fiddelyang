import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderRegionComponent } from './header-region.component';

describe('HeaderRegionComponent', () => {
  let component: HeaderRegionComponent;
  let fixture: ComponentFixture<HeaderRegionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderRegionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
