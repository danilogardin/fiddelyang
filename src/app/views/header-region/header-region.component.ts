import { Component, OnInit } from '@angular/core';
import { FormControl } from "@angular/forms";
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { CityService } from '../../services/city.service';
import { commonUser } from '../../base/common';

@Component({
	selector: 'app-header-region',
	templateUrl: './header-region.component.html',
	styleUrls: ['./header-region.component.css']
})
export class HeaderRegionComponent implements OnInit {

	constructor( private _CityService : CityService, public _commonUser : commonUser) {
		//console.log('header region');
	}


	myControl = new FormControl();
	options: any[];
	filteredOptions: Observable<string[]>;
  
	ngOnInit() {
		this._commonUser.regionInit();
		
		this.filteredOptions = this.myControl.valueChanges
		.pipe(
			startWith(''),
			map(value => this._filter(value))
		);
	}
  
	private _filter(value: string): string[] {
		const filterValue = value.toLowerCase();
		if (this.options != null){ 
			return this.options.filter(option => option.city1.toLowerCase().includes(filterValue));
		}
	}

	selectCity(o: any){
		this._commonUser.setUserCity(o);
	}

	getServiceCity(event: KeyboardEvent){
		let busca = (<HTMLInputElement>event.target).value;
		this._CityService.list(busca).subscribe(
			data => {
				this.options = data;
			}
		);
	}

}
