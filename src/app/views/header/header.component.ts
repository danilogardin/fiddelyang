import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { commonUser } from '../../base/common';

declare var $: any;
@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent {

	searchTerms: string;

	constructor( private router: Router, public _commonUser : commonUser) { }

	ngOnInit() {
		this.mobile();
   }


	searchOnKeyUp(event: KeyboardEvent) {
		//console.log(event);
		this.searchTerms = (<HTMLInputElement>event.target).value;
	}

	searchExecute(word: string) {
		this.searchTerms = word;
		this.router.navigate([`/busca/${this.searchTerms}`])
	}

		mobile(){
			if ($('.js-hamburger')[0]) {
				var button = $('.js-hamburger'),
					menu   = $('.js-hamburger-content');
				if(button.length > 0){
					button.on('click', function(e){
						e.preventDefault();
						button.toggleClass('is-active');
						menu.stop().slideToggle('slow');
					});
				}
			}
		}
	

}
