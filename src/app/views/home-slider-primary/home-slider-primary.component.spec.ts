import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeSliderPrimaryComponent } from './home-slider-primary.component';

describe('HomeSliderPrimaryComponent', () => {
  let component: HomeSliderPrimaryComponent;
  let fixture: ComponentFixture<HomeSliderPrimaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeSliderPrimaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeSliderPrimaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
