import { Component, OnInit } from '@angular/core';

import { ListBannerService } from '../../services/list-banner.service';
import { commonUser } from '../../base/common';

declare var $: any;

@Component({
	selector: 'app-home-slider-primary',
	templateUrl: './home-slider-primary.component.html',
	styleUrls: ['./home-slider-primary.component.css']
})
export class HomeSliderPrimaryComponent implements OnInit {

	constructor( private _ListBannerService : ListBannerService, public _commonUser : commonUser) {
		//console.log('header region');
	}

	sliderOptions: any;

	ngOnInit() { 

		this.getServiceBanner();
		// this.starSlide();
	}



	getServiceBanner(){

        this._ListBannerService.listBanner().subscribe(
            data => {
			
			   this.sliderOptions = data.banner;
			  this.starSlide();
            }
        );
	}
		
	starSlide(){

		

		setTimeout(function(){
			var element = $('.js-primary-slider');
			if(element.length > 0){
				element.slick({
					arrows:         false,
					dots:           true,
					infinite:       true,
					speed:          800,
					lazyLoad:       'ondemand'
				});
			}
		});
	}
}
