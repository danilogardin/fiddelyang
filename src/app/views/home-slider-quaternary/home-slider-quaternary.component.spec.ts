import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeSliderQuaternaryComponent } from './home-slider-quaternary.component';

describe('HomeSliderQuaternaryComponent', () => {
  let component: HomeSliderQuaternaryComponent;
  let fixture: ComponentFixture<HomeSliderQuaternaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeSliderQuaternaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeSliderQuaternaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
