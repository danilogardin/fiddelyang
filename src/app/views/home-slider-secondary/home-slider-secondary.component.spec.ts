import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeSliderSecondaryComponent } from './home-slider-secondary.component';

describe('HomeSliderSecondaryComponent', () => {
  let component: HomeSliderSecondaryComponent;
  let fixture: ComponentFixture<HomeSliderSecondaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeSliderSecondaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeSliderSecondaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
