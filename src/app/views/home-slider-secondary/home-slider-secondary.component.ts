import { Component, OnInit } from '@angular/core';

import { ListBannerService } from '../../services/list-banner.service';
import { commonUser } from '../../base/common';

declare var $: any;

@Component({
	selector: 'app-home-slider-secondary',
	templateUrl: './home-slider-secondary.component.html',
	styleUrls: ['./home-slider-secondary.component.css']
})
export class HomeSliderSecondaryComponent implements OnInit {


	constructor( private _ListBannerService : ListBannerService, public _commonUser : commonUser) {
		//console.log('header region');
	}
	
	sliderItems;
	sliderOptions: any;

	ngOnInit() {

		this.getServiceBanner();
	
	}

	getServiceBanner(){

        this._ListBannerService.listBanner().subscribe(
            data => {

				this.sliderOptions = data.company.filter(data => data.logoImgPath);
				console.log(this.sliderOptions)
			  	this.starSlide();
				//   this.sliderOptions = {Url: "https://s3-sa-east-1.amazonaws.com/fiddely"};
            }
        );
	}

	starSlide(){

		this.sliderItems = [1,2,3,4,5];

		let element = $('.js-secundary-slider');
		
		setTimeout(function(){
			if(element.length > 0){
				element.slick({
					arrows:         true,
					dots:           false,
					infinite:       true,
					centerMode:     true,
					variableWidth:  true,
					centerPadding:  '',
					slidesToShow:   5,
					slidesPerRow:   5,
					prevArrow:      '<svg class="slick-prev"><use xlink:href="assets/images/svg.svg#left"></svg>',
					nextArrow:      '<svg class="slick-next"><use xlink:href="assets/images/svg.svg#right"></svg>',
					lazyLoad:       'ondemand',
					responsive: [
						{
						breakpoint: 768,
						settings: {
							slidesToShow: 3
						}
						}, {
						breakpoint: 480,
						settings: {
							slidesToShow: 1
						}
						}
					]
				});
			}
		});
	}

}
