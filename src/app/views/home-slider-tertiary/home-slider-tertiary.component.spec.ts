import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeSliderTertiaryComponent } from './home-slider-tertiary.component';

describe('HomeSliderTertiaryComponent', () => {
  let component: HomeSliderTertiaryComponent;
  let fixture: ComponentFixture<HomeSliderTertiaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeSliderTertiaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeSliderTertiaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
