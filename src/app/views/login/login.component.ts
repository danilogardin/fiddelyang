import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { commonUser } from '../../base/common';
import { Login } from '../../models/login.model';
import { User } from '../../models/user.model';
import { LoginService } from '../../services/login.service';


declare var fechaModal : any;

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent {
	
	constructor( private _LoginService : LoginService, private router: Router, public _commonUser: commonUser ) {
		
	}

	lstlogin: Login[];
	lstUser = new User();

	onLogin() {
		if (!this._commonUser.getUserLoginState()) {
			//this.createUser();
			this._LoginService.onLogin({'pCPF': '12112112155', 'pFacebook': null, 'pPassword': '123456', 'pEmail': null }).subscribe(
				data => {
					this.lstlogin = data;
					this.createUser();
				}
			);
		} else {
			console.log('exist');
			console.log('check expire user');
		}		
		/**/
	}

	createUser() {
		this.lstUser.token = 'XPTO-123';
		this.lstUser.name = 'Fulano da Silva';
		this.lstUser.cityChanged = 'Jundiaí';
		this.lstUser.thumbnail = 'prevent.jpg';
		this.lstUser.expire = '12/02/2020';

		this._commonUser.setUserClientStorage(this.lstUser);

		this._commonUser.setUserCity(this.lstUser.cityChanged);

		fechaModal('#modalLogin');
		this._commonUser.setUserLogged();
	}

	onClickSign(){
		fechaModal('#modalLogin');
		this.router.navigate(['/cadastro']);

	}

}
