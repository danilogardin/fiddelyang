import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionsDetachComponent } from './promotions-detach.component';

describe('PromotionsDetachComponent', () => {
  let component: PromotionsDetachComponent;
  let fixture: ComponentFixture<PromotionsDetachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionsDetachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionsDetachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
