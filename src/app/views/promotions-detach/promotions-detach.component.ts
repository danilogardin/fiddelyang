import { Component, OnInit } from '@angular/core';
import { commonUser } from '../../base/common';

declare var $: any;
@Component({
  selector: 'app-promotions-detach',
  templateUrl: './promotions-detach.component.html',
  styleUrls: ['./promotions-detach.component.css']
})
export class PromotionsDetachComponent implements OnInit {

  constructor(public _commonUser : commonUser) { 

  }

sliderItems;

	ngOnInit() {
		 this.starSlide();
	}

  

  	starSlide(){

		
		console.log('chamar serviço para listar lojas slider secundario.');

		let element = $('.js-secundary-slider');
		
		setTimeout(function(){
			if(element.length > 0){
				element.slick({
					arrows:         true,
					dots:           false,
					infinite:       true,
					centerMode:     true,
					variableWidth:  true,
					centerPadding:  '',
					slidesToShow:   5,
					slidesPerRow:   5,
					prevArrow:      '<svg class="slick-prev"><use xlink:href="assets/images/svg.svg#left"></svg>',
					nextArrow:      '<svg class="slick-next"><use xlink:href="assets/images/svg.svg#right"></svg>',
					lazyLoad:       'ondemand',
					responsive: [
						{
						breakpoint: 768,
						settings: {
							slidesToShow: 3
						}
						}, {
						breakpoint: 480,
						settings: {
							slidesToShow: 1
						}
						}
					]
				});
			}
		});
	}
}
