import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  search;

  grid: boolean = true;
  list: boolean = false

  value;

  clickEventGrid(){
    this.grid = true;
    this.list = false;
  }

  clickEventList(){
      this.grid = false;
      this.list = true;      
  }

  clickList(){
    var diplaySeach = document.getElementById('diplaySeach');
    if(diplaySeach.dataset.type != 'list'){
        diplaySeach.dataset.type = 'list';
    }
    
  }

  clickGrid(){
    var diplaySeach = document.getElementById('diplaySeach');
    if(diplaySeach.dataset.type != 'grid'){
        diplaySeach.dataset.type = 'grid';
    }
  }
  

  constructor(public route: ActivatedRoute) { }

    ngOnInit() {

        this.search = this.route.params;

        this.advancedSearch();

        let selfThis = this;
        this.responseSeach();

        $(window).on('resize', function(){
            selfThis.responseSeach();
        });

    }

    advancedSearch(){
        var searchButton  = $('.js-advanced-search-buttonAng'),
        searchContent = $('.js-advanced-search-contentAng');
        if (searchContent[0]) {
            searchButton.click(function(){
                searchContent.slideToggle(300);
                return false;
            })
        }
}

    function(){
        // COMBO UF SELECIONA - GET SERVICE CATEGORIA
        if ($('.js-combo-uf')[0]) {
            $('.js-combo-uf').on('change', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var paramUF = $(this).val();
                $.getJSON('/Service/ListCity_S', { state_id: ''+paramUF+'' }).done(function (data) {
                    var htmlOptions = "";
                    data.map(function(e){
                        htmlOptions += "<option value='"+e.Value+"'>"+e.Text+"</option>"
                    });
                    $('.js-combo-cidade').html(htmlOptions);
                    if (paramUF == 'SP') {
                        $('.js-combo-cidade').val('4845');
                    }
                });
            });
        }

        // CATEGORIAS FOOTER
        if ($('.js-service-category')[0]) {
            $.getJSON('/Service/ListCategory_S').done(function (data) {
                var htmlOptions = '';
                data.map(function(e){
                    htmlOptions += '<li><a class="footer__links__category" href="#">'+e.category+'</a></li>'
                });
                $('.js-service-category').html(htmlOptions);
            });
        }




    };

	//responsivo de busca
	responseSeach(){
        var diplaySeach = document.getElementById('diplaySeach');
        		
		if (window.innerWidth <= 800) {
			diplaySeach.dataset.type = 'grid';
			
            this.grid = true;
            this.list = false;

			
		}
    }

}
