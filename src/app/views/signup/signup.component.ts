import { Component, OnInit } from '@angular/core';
import { SignupService } from '../../services/signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  
  constructor( private _SignupService : SignupService) { }

  f: any;

  ngOnInit() {
  }

  onSubmit(f){
      f.form.value.facebookId = '';
      f.form.value.token = '';
      f.form.value.hash = '';
      f.form.value.cityId = 0;

      this._SignupService.setSignUp(f.form.value).subscribe(
				data => {
          console.log('resultado');
          console.log(data);
				}
			);

      //console.log(f.form.value);
      //console.log('CPF ' + f.form.value.cpf);
      return false;
  }

}
