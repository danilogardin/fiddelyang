import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreCommentsComponent } from './store-comments.component';

describe('StoreCommentsComponent', () => {
  let component: StoreCommentsComponent;
  let fixture: ComponentFixture<StoreCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
