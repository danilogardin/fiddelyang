import { Component, OnInit } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-store-home',
  templateUrl: './store-home.component.html',
  styleUrls: ['./store-home.component.css']
})
export class StoreHomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.comments();
  }

  comments(){

		var element = $('.js-comments-slider');

		// Verifica se existe algum slider na página
		if(element.length > 0){

			element.slick({
				arrows:         true,
				dots:           false,
				infinite:       true,
				centerMode:     false,
				slidesToShow:   3,
                prevArrow:      '<svg class="slick-prev"><use xlink:href="assets/images/svg.svg#left"></svg>',
				nextArrow:      '<svg class="slick-next"><use xlink:href="assets/images/svg.svg#right"></svg>',
				responsive: [
					{
					  breakpoint: 991,
					  settings: {
						arrows:       false,
						dots:         true,
						slidesToShow: 2
					  }
					}, {
					  breakpoint: 600,
					  settings: {
						arrows:       false,
						dots:         true,
						slidesToShow: 1
					  }
					}
				]
			});

		}

  }
  
}
