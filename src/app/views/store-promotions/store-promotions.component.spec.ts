import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorePromotionsComponent } from './store-promotions.component';

describe('StorePromotionsComponent', () => {
  let component: StorePromotionsComponent;
  let fixture: ComponentFixture<StorePromotionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorePromotionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorePromotionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
