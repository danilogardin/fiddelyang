import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatCheckboxClickAction } from '@angular/material';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {

  storeId;

  status: boolean = true;

  clickEventActive(){
    this.status = true;
  }

  clickEvent(){
      this.status = false;       
  }
  
  constructor(public route: ActivatedRoute) { }

  ngOnInit() {
    this.storeId = this.route.params;
    
    console.log('Loja ID ' + this.route.params);
    console.log('chamamos servico para descobrir se existe');
  }

}
