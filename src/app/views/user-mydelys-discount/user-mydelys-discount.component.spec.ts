import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMydelysDiscountComponent } from './user-mydelys-discount.component';

describe('UserMydelysDiscountComponent', () => {
  let component: UserMydelysDiscountComponent;
  let fixture: ComponentFixture<UserMydelysDiscountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMydelysDiscountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMydelysDiscountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
