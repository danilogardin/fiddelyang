import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMydelysListComponent } from './user-mydelys-list.component';

describe('UserMydelysListComponent', () => {
  let component: UserMydelysListComponent;
  let fixture: ComponentFixture<UserMydelysListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMydelysListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMydelysListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
