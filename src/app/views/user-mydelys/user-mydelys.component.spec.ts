import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMydelysComponent } from './user-mydelys.component';

describe('UserMydelysComponent', () => {
  let component: UserMydelysComponent;
  let fixture: ComponentFixture<UserMydelysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMydelysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMydelysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
