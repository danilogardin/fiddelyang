import { Component, OnInit } from '@angular/core';
import { log } from 'util';

declare var $: any;
@Component({
  selector: 'app-user-mydelys',
  templateUrl: './user-mydelys.component.html',
  styleUrls: ['./user-mydelys.component.css']
})
export class UserMydelysComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.response();
    this.users();

  }

  
    // Method to init knob
    // dependence of plugins/mask.js
    users(){
  
      var element = $('.js-knob');
  
      // Verify if element exists.
      if(element.length > 0){
        element.knob({
          change : function (value) {
            //console.log("change : " + value);
          },
          release : function (value) {
            console.log("release : " + value);
          },
          cancel : function () {
            console.log("cancel : ", this);
          },
          draw : function () {
  
            // "tron" case
            if(this.$.data('skin') == 'tron') {
  
              this.cursorExt = 0.3;
  
              var a = this.arc(this.cv)  // Arc
                , pa                   // Previous arc
                , r = 1;
  
              this.g.lineWidth = this.lineWidth;
  
              if (this.o.displayPrevious) {
                pa = this.arc(this.v);
                this.g.beginPath();
                this.g.strokeStyle = this.pColor;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
                this.g.stroke();
              }
  
              this.g.beginPath();
              this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
              this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
              this.g.stroke();
  
              this.g.lineWidth = 2;
              this.g.beginPath();
              this.g.strokeStyle = this.o.fgColor;
              this.g.arc( this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
              this.g.stroke();
  
              return false;
            }
          }
        });
  
  
          }
  
    }

    response() {

      let response = document.getElementById('response');

      console.log( response.dataset.width);
      if (response != null){
        response.dataset.width = '380';
        response.dataset.height = '380';
    
        if (window.innerWidth <= 600) {
          response.dataset.width = '250';
          response.dataset.height = '250';
        }
        else{ return null}
      }
  
    }
  
  };
