import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRescuesComponent } from './user-rescues.component';

describe('UserRescuesComponent', () => {
  let component: UserRescuesComponent;
  let fixture: ComponentFixture<UserRescuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserRescuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRescuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
